/*
 * 
 */
package com.CreepersHelp.test;


import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

// TODO: Auto-generated Javadoc
/*
 *
  String key = java.util.Arrays.toString((UUID.randomUUID().toString() + UUID.randomUUID().toString()).replaceAll("-", "").split("(?<=\\G....)")).replaceAll(", ", "-");
    key = key.substring(1, key.length() - 1);
 *
 */

/**
 * The Class BitTest.
 */
public class BitTest {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		String registryURL = ("https://bitbucket.org/api/1.0/repositories/TyrannyCraft/bitbucketrest-java/issues/1/");
		String url=registryURL;
		try {
			HttpPut method=new HttpPut(url);
			method.setEntity(new StringEntity("title=5&content=2&status=wontfix&priority=blocker&kind=task"));
			method.addHeader(new BasicScheme().authenticate(new UsernamePasswordCredentials("creepers-help", "[nick6699]"), method, null));
			
			
			
		    ResponseHandler<String> handler=new BasicResponseHandler();
		    String responseBody=EntityUtils.toString(HttpClientBuilder.create().build().execute(method).getEntity());
		    System.err.println(responseBody);
		  }
		 catch (  Exception e) {
			 e.printStackTrace();
		  }
	}
}







































	