package com.CreepersHelp.bitbucket.authentication;

import java.security.Principal;

import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

import org.apache.http.auth.BasicUserPrincipal;
import org.apache.http.auth.Credentials;

public class OAuthCredentials extends CommonsHttpOAuthConsumer implements Credentials {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new oauth credentials.
	 *
	 * @param consumerKey the consumer key
	 * @param consumerSecret the consumer secret
	 */
	public OAuthCredentials(String consumerKey, String consumerSecret) {
		super(consumerKey, consumerSecret);
	}

	/* (non-Javadoc)
	 * @see org.apache.http.auth.Credentials#getUserPrincipal()
	 */
	@Override
	public Principal getUserPrincipal() {
		return new BasicUserPrincipal(getConsumerKey());
	}

	/* (non-Javadoc)
	 * @see org.apache.http.auth.Credentials#getPassword()
	 */
	@Override
	public String getPassword() {
		return getConsumerSecret();
	}

}
