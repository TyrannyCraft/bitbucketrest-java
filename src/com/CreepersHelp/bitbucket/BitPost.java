/*
 * 
 */
package com.CreepersHelp.bitbucket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;

// TODO: Auto-generated Javadoc
/**
 * The Class BitPost.
 */
public abstract class BitPost extends BitCore {

	/** The response. */
	private HttpResponse response;
	
	/** The sb. */
	private StringBuilder sb = null;
	
	/** The max redirects. */
	private int connectTimeout = 15000, connectRequestTimeout = 15000, maxRedirects = 10;
	
	/** The running. */
	private boolean running = false;

	/**
	 * Sets the connect timeout.
	 *
	 * @param connectTimeout the new connect timeout
	 */
	public void setConnectTimeout(int connectTimeout) {
		if (running) return;
		this.connectTimeout = connectTimeout;
	}
	
	/**
	 * Sets the connect request timeout.
	 *
	 * @param connectRequestTimeout the new connect request timeout
	 */
	public void setConnectRequestTimeout(int connectRequestTimeout) {
		if (running) return;
		this.connectRequestTimeout = connectRequestTimeout;
	}
	
	/**
	 * Sets the max redirects.
	 *
	 * @param maxRedirects the new max redirects
	 */
	public void setMaxRedirects(int maxRedirects) {
		if (running) return;
		this.maxRedirects = maxRedirects;
	}
	
	/**
	 * Get the response as String.
	 *
	 * @return the response
	 */
	public String getResponse() {
		if (running) return null;
		
		if (sb == null) return null;
		return sb.toString();
	}
	
	/**
	 * Execute this POST request.
	 */
	public synchronized void execute() {
		if (running) return;
		running = true;
		
		this.response = null;
		this.sb = null;
		this.close();
		
		if (!stringHasValue(getUrl()))
			throw new UnsupportedOperationException("getUrl() returned null! You must specify a URL.");
		
		try {
			this.open();
			
			HttpPost postRequest = new HttpPost(getUrl());
			
			postRequest.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setMaxRedirects(maxRedirects).setConnectTimeout(connectTimeout).build());
			
			if (getCreds() != null)
				if (getCreds() instanceof oauth.signpost.OAuthConsumer) {
					((oauth.signpost.OAuthConsumer)getCreds()).sign(postRequest);
				} else {
					if (stringHasValue(getCreds().getPassword()))
						postRequest.addHeader(new BasicScheme().authenticate(getCreds(), postRequest, null));
				}
			
			postRequest.setEntity(getInput());
			
			this.response = getHttpClient().execute(postRequest);


			try {
				StringBuilder sb = new StringBuilder();
				BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			 
				String output;
				
				while ((output = br.readLine()) != null) {
					sb.append(output);
				}
				
				this.sb = sb;
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
			postRequest.releaseConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {  
			this.close();
		}
		
		running = false;
	}
	
	/**
	 * This is to parse the input.
	 *
	 * @return the input
	 * @throws Exception the exception
	 */
	public StringEntity getInput() throws Exception {return null;}
}
