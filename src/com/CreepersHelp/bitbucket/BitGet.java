/*
 * 
 */
package com.CreepersHelp.bitbucket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;

// TODO: Auto-generated Javadoc
/**
 * The Class BitGet.
 */
public abstract class BitGet extends BitCore {

	/** The response. */
	private HttpResponse response;
	
	/** The sb. */
	private StringBuilder sb = null;
	
	/** The max redirects. */
	private int connectTimeout = 15000, connectRequestTimeout = 15000, maxRedirects = 10;
	
	/** The running. */
	private boolean running = false;

	/**
	 * Sets the connect timeout.
	 *
	 * @param connectTimeout the new connect timeout
	 */
	public void setConnectTimeout(int connectTimeout) {
		if (running) return;
		this.connectTimeout = connectTimeout;
	}
	
	/**
	 * Sets the connect request timeout.
	 *
	 * @param connectRequestTimeout the new connect request timeout
	 */
	public void setConnectRequestTimeout(int connectRequestTimeout) {
		if (running) return;
		this.connectRequestTimeout = connectRequestTimeout;
	}
	
	/**
	 * Sets the max redirects.
	 *
	 * @param maxRedirects the new max redirects
	 */
	public void setMaxRedirects(int maxRedirects) {
		if (running) return;
		this.maxRedirects = maxRedirects;
	}
	
	/**
	 * Get the response as String.
	 *
	 * @return the response
	 */
	public String getResponse() {
		if (running) return null;
		
		if (sb == null) return null;
		return sb.toString();
	}
	
	/**
	 * Execute this GET request.
	 */
	public synchronized void execute() {
		if (running) return;
		running = true;
		
		this.response = null;
		this.sb = null;
		this.close();
		
		if (!stringHasValue(getUrl()))
			throw new UnsupportedOperationException("getUrl() returned null! You must specify a URL.");
		
		try {
			this.open();
			
			HttpGet getRequest = new HttpGet(getUrl());
			
			getRequest.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setMaxRedirects(maxRedirects).setConnectTimeout(connectTimeout).build());
			
			if (getCreds() != null)
				if (getCreds() instanceof oauth.signpost.OAuthConsumer) {
					((oauth.signpost.OAuthConsumer)getCreds()).sign(getRequest);
				} else {
					if (stringHasValue(getCreds().getPassword()))
						getRequest.addHeader(new BasicScheme().authenticate(getCreds(), getRequest, null));
				}
			
			this.response = getHttpClient().execute(getRequest);


			try {
				StringBuilder sb = new StringBuilder();
				BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			 
				String output;
				
				while ((output = br.readLine()) != null) {
					sb.append(output);
				}
				
				this.sb = sb;
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
			getRequest.releaseConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {  
			this.close();
			running = false;
		}
		
		running = false;
	}
}
