/*
 * 
 */
package com.CreepersHelp.bitbucket.exceptions;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;

import com.CreepersHelp.bitbucket.authentication.OAuthCredentials;
import com.CreepersHelp.bitbucket.issues.post.BitIssueCreator;

// TODO: Auto-generated Javadoc
/**
 * The Class UncaughtExceptionHandler.
 */
public class UncaughtExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {

	/** The repo slug. */
	private String repoOwner, repoSlug;
	
	/** The creds. */
	private Credentials creds;
	
	/** The print stack. */
	private boolean printStack;

	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param printStack the print stack
	 */
	private UncaughtExceptionHandler(String repoOwner, String repoSlug, boolean printStack) {
		this.repoOwner = repoOwner;
		this.repoSlug = repoSlug;
		this.printStack = printStack;
	}
	
	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param userName the user name
	 * @param password the password
	 */
	public UncaughtExceptionHandler(String repoOwner, String repoSlug, String userName, String password) {
		this(repoOwner, repoSlug, false);
		this.creds = new UsernamePasswordCredentials(userName, password);
	}
	
	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param oa the oa
	 */
	public UncaughtExceptionHandler(String repoOwner, String repoSlug, OAuthCredentials oa) {
		this(repoOwner, repoSlug, false);
		this.creds = oa;
	}
	
	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param ca the ca
	 */
	public UncaughtExceptionHandler(String repoOwner, String repoSlug, Credentials ca) {
		this(repoOwner, repoSlug, false);
		this.creds = ca;
	}
	
	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param userName the user name
	 * @param password the password
	 * @param printStack the print stack
	 */
	public UncaughtExceptionHandler(String repoOwner, String repoSlug, String userName, String password, boolean printStack) {
		this(repoOwner, repoSlug, printStack);
		this.creds = new UsernamePasswordCredentials(userName, password);
	}
	
	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param oa the oa
	 * @param printStack the print stack
	 */
	public UncaughtExceptionHandler(String repoOwner, String repoSlug, OAuthCredentials oa, boolean printStack) {
		this(repoOwner, repoSlug, printStack);
		this.creds = oa;
	}
	
	/**
	 * Instantiates a new uncaught exception handler.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 * @param ca the ca
	 * @param printStack the print stack
	 */
	public UncaughtExceptionHandler(String repoOwner, String repoSlug, Credentials ca, boolean printStack) {
		this(repoOwner, repoSlug, printStack);
		this.creds = ca;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
	 */
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		try {
			ExceptionsHandler exH = new ExceptionsHandler(t, e);
			
			BitIssueCreator bic = new BitIssueCreator(repoOwner, repoSlug);
			bic.setAuthentication(creds);
			bic.setTitle(!exH.isSubClass() ? "Error in class: " + exH.getErrorClass() : "Error in class: " + exH.getErrorClass() + " (Subclass of: " + exH.getPhisicalClass() + ")");
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("```\n")
			  .append("#!java\n")
			  .append(exH.getStackTraceAsString())
			  .append("\n\n")
			  .append("```");
			
			bic.setBodyContent(sb.toString());
			
			bic.execute();
			
			if (printStack)
				e.printStackTrace();
			
		} catch (Throwable ex) {
			e.printStackTrace();
			ex.printStackTrace();
		}
	}

}
