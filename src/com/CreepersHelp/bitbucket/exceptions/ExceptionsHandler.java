/*
 * 
 */
package com.CreepersHelp.bitbucket.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

// TODO: Auto-generated Javadoc
/**
 * The Class ExceptionsHandler.
 */
public final class ExceptionsHandler {
	
	/** The e. */
	private final Throwable e;
	
	/** The t. */
	private final Thread t;
	
	/** The stack trace. */
	private final String stackTrace;
	
	/**
	 * Instantiates a new exceptions handler.
	 *
	 * @param e the e
	 */
	public ExceptionsHandler(Throwable e) {
		this(Thread.currentThread(), e);
	}
	
	/**
	 * Instantiates a new exceptions handler.
	 *
	 * @param t the t
	 * @param e the e
	 */
	public ExceptionsHandler(Thread t, Throwable e) {
		this.e = e;

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		e.printStackTrace(pw);
		
		stackTrace = sw.getBuffer().toString();
		this.t = t;
	}
	
	/**
	 * Gets the error class.
	 *
	 * @return the error class
	 */
	public final String getErrorClass() {
		final String stackTrace = getStackTraceAsString();
		int i = 1;
		String firstLine = stackTrace.split("\n")[i].replace("\t", "");
		while (firstLine.startsWith("at java.") || firstLine.startsWith("at javax.") || firstLine.startsWith("at oracle.") || firstLine.startsWith("at jdk.") || firstLine.startsWith("at sun.")) {
			try {
				i++;
				firstLine = stackTrace.split("\n")[i].replace("\t", "");
				if (i > stackTrace.split("\n").length)
					break;
			} catch (Throwable t) {
				firstLine = stackTrace.split("\n")[1].replace("\t", "");
				break;
			}
		}
		String clazz = "null";
		
		if (firstLine.contains("$")) {
			String[] clazzes = firstLine.split("\\$");
			clazz = clazzes[clazzes.length - 1].split("\\(")[0].split("\\.")[0];
		} else 
			clazz = firstLine.split("\\(")[1].split("\\)")[0].split(".java")[0];
		
		return clazz;
	}
	
	/**
	 * Checks if is sub class.
	 *
	 * @return true, if is sub class
	 */
	public final boolean isSubClass() {
		final String stackTrace = getStackTraceAsString();
		int i = 1;
		String firstLine = stackTrace.split("\n")[i].replace("\t", "");
		while (firstLine.startsWith("at java.") || firstLine.startsWith("at javax.") || firstLine.startsWith("at oracle.") || firstLine.startsWith("at jdk.") || firstLine.startsWith("at sun.")) {                                    
			try {
				i++;
				firstLine = stackTrace.split("\n")[i].replace("\t", "");
				if (i > stackTrace.split("\n").length)
					break;
			} catch (Throwable t) {
				firstLine = stackTrace.split("\n")[1].replace("\t", "");
				break;
			}
		}
		
		if (firstLine.contains("$")) 
			return true;
		
		return false;
	}
	
	/**
	 * Gets the phisical class.
	 *
	 * @return the phisical class
	 */
	public final String getPhisicalClass() {
		final String stackTrace = getStackTraceAsString();
		int i = 1;
		String firstLine = stackTrace.split("\n")[i].replace("\t", "");
		while (firstLine.startsWith("at java.") || firstLine.startsWith("at javax.") || firstLine.startsWith("at oracle.") || firstLine.startsWith("at jdk.") || firstLine.startsWith("at sun.")) {
			try {
				i++;
				firstLine = stackTrace.split("\n")[i].replace("\t", "");
				if (i > stackTrace.split("\n").length)
					break;
			} catch (Throwable t) {
				firstLine = stackTrace.split("\n")[1].replace("\t", "");
				break;
			}
		}
		String clazz = "null";
		
			clazz = firstLine.split("\\(")[1].split("\\)")[0].split(".java")[0];
		
		return clazz;
	}
	
	/**
	 * Gets the stack trace as string.
	 *
	 * @return the stack trace as string
	 */
	public final String getStackTraceAsString() {
		return stackTrace;
	}

	
	/**
	 * Gets the throwable.
	 *
	 * @return the throwable
	 */
	public final Throwable getThrowable() {
		return e;
	}

	/**
	 * Gets the thread.
	 *
	 * @return the thread
	 */
	public final Thread getThread() {
		return t;
	}
}
