package com.CreepersHelp.bitbucket;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.CreepersHelp.bitbucket.authentication.OAuthCredentials;

/**
 * The Class BitCore.
 */
public abstract class BitCore {
	
	/** Base URL for REST api. */
	public final static String baseUrl = "https://bitbucket.org/api";
	
	/** The creds. */
	private Credentials creds = null;
	
	/** The http client. */
	private CloseableHttpClient httpClient;

	/**
	 * Create a new BitCore instance.
	 */
	public BitCore() {}
	
	/**
	 * Close all connections for this instance.
	 */
	public synchronized void close() {
		try {
			getHttpClient().close();
			this.httpClient = null;
		} catch (Throwable e) {}
	}
	
	/**
	 * Open a new HttpClient connection for this instance.
	 */
	public synchronized void open() {
		if (getHttpClient() != null)
			throw new UnsupportedOperationException("You must close a connection before opening a new.");
		
		this.httpClient = HttpClientBuilder.create().build();
	}
	
	/**
	 * Get the HttpClient for this instance.
	 *
	 * @return the http client
	 */
	public CloseableHttpClient getHttpClient() {
		return this.httpClient;
	}
	
	/**
	 * Get the credentials for this instance for BASIC Authentication.
	 *
	 * @return the creds
	 */
	public Credentials getCreds() {
		return creds;
	}
	
	/**
	 * Set the authentication for this instance.
	 *
	 * @param user the user
	 * @param pass the pass
	 */
	public void setAuthentication(String user, String pass) {
		this.creds = new UsernamePasswordCredentials(user, pass);
	}
	
	/**
	 * Set the authentication for this instance.
	 *
	 * @param consumerKey the consumer key
	 * @param consumerSecret the consumer secret
	 */
	public void setOAuthAuthentication(String consumerKey, String consumerSecret) {
		this.creds = new OAuthCredentials(consumerKey, consumerSecret);
	}
	
	/**
	 * Set the authentication for this instance.
	 *
	 * @param creds the new o auth authentication
	 */
	public void setOAuthAuthentication(OAuthCredentials creds) {
		this.creds = creds;
	}
	
	/**
	 * Set the authentication for this instance.
	 *
	 * @param creds the new authentication
	 */
	public void setAuthentication(Credentials creds) {
		this.creds = creds;
	}
	
	/**
	 * Check if a sting has value.
	 *
	 * @param str the str
	 * @return true, if successful
	 */
	public boolean stringHasValue(String str) {
		return str == null ? false : str.length() > 0 && !str.isEmpty();
	}
	
	/**
	 * Get the api URL.
	 *
	 * @return the url
	 */
	public abstract String getUrl();
	
	
	/**
	 * Replace all unsupported characters to supported data values.
	 *
	 * @param s the s
	 * @return the string
	 */
	public final String replaceBad(String s) {
		return s;
	}
}