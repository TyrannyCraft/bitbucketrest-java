/*
 * 
 */
package com.CreepersHelp.bitbucket.issues.get;

// TODO: Auto-generated Javadoc
/**
 * The Class BitIssueFollowers.
 */
public class BitIssueFollowers extends BitGetIssue {

	/** The issue id. */
	private String issueId = null; 
	
	/**
	 * Instantiates a new bit issue followers.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 */
	public BitIssueFollowers(String repoOwner, String repoSlug) {
		this.setRepoOwner(repoOwner);
		this.setRepoSlug(repoSlug);
	}
	
	/**
	 * Sets the issue id.
	 *
	 * @param issueId the new issue id
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitCore#getUrl()
	 */
	public String getUrl() {
		return (baseUrl + "/1.0/repositories/" + getRepoOwner() + "/" + getRepoSlug() + "/issues/" + issueId + "/");
	}
}
