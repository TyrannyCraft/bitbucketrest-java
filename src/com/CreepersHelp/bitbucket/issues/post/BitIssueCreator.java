/*
 * 
 */
package com.CreepersHelp.bitbucket.issues.post;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class BitIssueCreator.
 */
public class BitIssueCreator extends BitPostIssue {
	
	/** The content. */
	private String content = null; 
	
	/** The status. */
	private Status status = Status.NEW;
	
	/** The priority. */
	private Priority priority = Priority.TRIVIAL;
	
	/** The kind. */
	private Kind kind = Kind.BUG;
	
	/** The title. */
	private String responsible, milestone, component, version, title = "Title Filler";
	
	/**
	 * Instantiates a new bit issue creator.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 */
	public BitIssueCreator(String repoOwner, String repoSlug) {
		this.setRepoOwner(repoOwner);
		this.setRepoSlug(repoSlug);
	}
	
	/**
	 * Set the content of this issue.
	 *
	 * @param content the new body content
	 */
	public void setBodyContent(String content) {
		this.content = content;
	}
	
	/**
	 * Set the title of this issue.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Set the status of this issue.
	 *
	 * @param status <code>Status</code>
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Set the priority of this issue.
	 *
	 * @param priority <code>Priority</code>
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	
	/**
	 * Set the kind of this issue.
	 *
	 * @param kind <code>Kind</code>
	 */
	public void setKind(Kind kind) {
		this.kind = kind;
	}
	
	/**
	 * Set the person responsible for this issue.
	 *
	 * @param responsible the new responsible
	 */
	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}
	
	/**
	 * Set the person responsible for this issue.
	 *
	 * @param responsible the new assignee
	 */
	public void setAssignee(String responsible) {
		this.responsible = responsible;
	}
	
	/**
	 * Set the milestone of this issue.
	 *
	 * @param milestone the new milestone
	 */
	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}
	
	/**
	 * Set the component of this issue.
	 *
	 * @param component the new component
	 */
	public void setComponent(String component) {
		this.component = component;
	}
	
	/**
	 * Set the version of this issue.
	 *
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitCore#getUrl()
	 */
	public String getUrl() {
		return (baseUrl + "/1.0/repositories/" + getRepoOwner() + "/" + getRepoSlug() + "/issues/");
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitPost#getInput()
	 */
	public StringEntity getInput() throws UnsupportedEncodingException {
		
		
		StringBuilder inputs = new StringBuilder();
		{
			{
				inputs.append("title=" + replaceBad(title));
				inputs.append("&");
			}
			{
				inputs.append("content="+replaceBad(content));
				inputs.append("&");
			}
			{
				inputs.append("status="+status.value);
				inputs.append("&");
			}
			{
				inputs.append("priority="+priority.value);
				inputs.append("&");
			}
			{
				inputs.append("kind="+kind.value);
				inputs.append("&");
			}
			
			if (stringHasValue(responsible))
			{
				inputs.append("responsible="+replaceBad(responsible));
				inputs.append("&");
			}
			if (stringHasValue(milestone))
			{
				inputs.append("milestone="+replaceBad(milestone));
				inputs.append("&");
			}
			if (stringHasValue(component))
			{
				inputs.append("component="+replaceBad(component));
				inputs.append("&");
			}
			if (stringHasValue(version))
			{
				inputs.append("version="+replaceBad(version));
				inputs.append("&");
			}
		}
		
		return new StringEntity(inputs.toString());
	}
	
	/**
	 * The Enum Status.
	 */
	public static enum Status {
		
		/** The new. */
		NEW("new"),
		
		/** The open. */
		OPEN("open"),
		
		/** The resolved. */
		RESOLVED("resolved"),
		
		/** The on hold. */
		ON_HOLD("on hold"),
		
		/** The invalid. */
		INVALID("invalid"),
		
		/** The duplicate. */
		DUPLICATE("duplicate"),
		
		/** The wontfix. */
		WONTFIX("wontfix");
		
		/** The value. */
		private final String value;
		
		/**
		 * Instantiates a new status.
		 *
		 * @param s the s
		 */
		private Status(String s) {
			this.value = s.toLowerCase();
		}		
	}
	
	/**
	 * The Enum Priority.
	 */
	public static enum Priority {
		
		/** The trivial. */
		TRIVIAL("trivial"),
		
		/** The minor. */
		MINOR("minor"),
		
		/** The major. */
		MAJOR("major"),
		
		/** The critical. */
		CRITICAL("critical"),
		
		/** The blocker. */
		BLOCKER("blocker");
		
		/** The value. */
		private final String value;
		
		/**
		 * Instantiates a new priority.
		 *
		 * @param s the s
		 */
		private Priority(String s) {
			this.value = s.toLowerCase();
		}		
	}
	
	/**
	 * The Enum Kind.
	 */
	public static enum Kind {
		
		/** The bug. */
		BUG("bug"),
		
		/** The enhancement. */
		ENHANCEMENT("enhancement"),
		
		/** The proposal. */
		PROPOSAL("propoal"),
		
		/** The task. */
		TASK("task");
		
		/** The value. */
		private final String value;
		
		/**
		 * Instantiates a new kind.
		 *
		 * @param s the s
		 */
		private Kind(String s) {
			this.value = s.toLowerCase();
		}
	}
}