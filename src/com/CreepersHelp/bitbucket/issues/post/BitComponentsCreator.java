/*
 * 
 */
package com.CreepersHelp.bitbucket.issues.post;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class BitComponentsCreator.
 */
public class BitComponentsCreator extends BitPostIssue {
	
	/** The content. */
	private String content = null; 
	
	/**
	 * Instantiates a new bit components creator.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 */
	public BitComponentsCreator(String repoOwner, String repoSlug) {
		this.setRepoOwner(repoOwner);
		this.setRepoSlug(repoSlug);
	}
	
	/**
	 * Sets the component name.
	 *
	 * @param name the new component name
	 */
	public void setComponentName(String name) {
		this.content = name;
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitCore#getUrl()
	 */
	public String getUrl() {
		return (baseUrl + "/1.0/repositories/" + getRepoOwner() + "/" + getRepoSlug() + "/issues/components/");
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitPost#getInput()
	 */
	public StringEntity getInput() throws UnsupportedEncodingException {
		return new StringEntity("name=" + replaceBad(content));
	}
}
