/*
 * 
 */
package com.CreepersHelp.bitbucket.issues.post;

import com.CreepersHelp.bitbucket.BitPost;

// TODO: Auto-generated Javadoc
/**
 * The Class BitPostIssue.
 */
abstract class BitPostIssue extends BitPost {

	/** The repo slug. */
	private String repoOwner, repoSlug;
	
	/**
	 * Set the repository owner.
	 *
	 * @param owner the new repo owner
	 */
	public void setRepoOwner(String owner) {
		this.repoOwner = owner;
	}
	
	/**
	 * Set the repository slug/ID.
	 *
	 * @param slug the new repo slug
	 */
	public void setRepoSlug(String slug) {
		this.repoSlug = slug;
	}

	/**
	 * Get the repository owner.
	 *
	 * @return the repo owner
	 */
	public String getRepoOwner() {
		return this.repoOwner;
	}
	
	/**
	 * Set the repository slug.
	 *
	 * @return the repo slug
	 */
	public String getRepoSlug() {
		return this.repoSlug;
	}
}