/*
 * 
 */
package com.CreepersHelp.bitbucket.issues.post;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class BitCommentCreator.
 */
public class BitCommentCreator extends BitPostIssue {
	
	/** The issue id. */
	private String content = null, issueId = null; 
	
	/**
	 * Instantiates a new bit comment creator.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 */
	public BitCommentCreator(String repoOwner, String repoSlug) {
		this.setRepoOwner(repoOwner);
		this.setRepoSlug(repoSlug);
	}
	
	/**
	 * Sets the issue id.
	 *
	 * @param issueId the new issue id
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	
	/**
	 * Sets the comment content.
	 *
	 * @param content the new comment content
	 */
	public void setCommentContent(String content) {
		this.content = content;
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitCore#getUrl()
	 */
	public String getUrl() {
		return (baseUrl + "/1.0/repositories/" + getRepoOwner() + "/" + getRepoSlug() + "/issues/" + issueId + "/comments/");
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitPost#getInput()
	 */
	public StringEntity getInput() throws UnsupportedEncodingException {
		return new StringEntity("name=" + replaceBad(content));
	}
}