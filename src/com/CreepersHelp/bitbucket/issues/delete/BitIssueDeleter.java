/*
 * 
 */
package com.CreepersHelp.bitbucket.issues.delete;

// TODO: Auto-generated Javadoc
/**
 * The Class BitIssueDeleter.
 */
public class BitIssueDeleter extends BitDeleteIssue {

	/** The issue id. */
	private String issueId = null; 
	
	/**
	 * Instantiates a new bit issue deleter.
	 *
	 * @param repoOwner the repo owner
	 * @param repoSlug the repo slug
	 */
	public BitIssueDeleter(String repoOwner, String repoSlug) {
		this.setRepoOwner(repoOwner);
		this.setRepoSlug(repoSlug);
	}
	
	/**
	 * Sets the issue id.
	 *
	 * @param issueId the new issue id
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	
	/* (non-Javadoc)
	 * @see com.CreepersHelp.bitbucket.BitCore#getUrl()
	 */
	public String getUrl() {
		return (baseUrl + "/1.0/repositories/" + getRepoOwner() + "/" + getRepoSlug() + "/issues/" + issueId + "/");
	}
}
