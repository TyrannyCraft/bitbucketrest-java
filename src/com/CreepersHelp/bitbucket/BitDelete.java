/*
 * 
 */
package com.CreepersHelp.bitbucket;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.auth.BasicScheme;

// TODO: Auto-generated Javadoc
/**
 * The Class BitDelete.
 */
public abstract class BitDelete extends BitCore {

	/** The response. */
	private HttpResponse response;
	
	/** The sb. */
	private StringBuilder sb = null;
	
	/** The max redirects. */
	private int connectTimeout = 15000, connectRequestTimeout = 15000, maxRedirects = 10;
	
	/** The running. */
	private boolean running = false;

	/**
	 * Sets the connect timeout.
	 *
	 * @param connectTimeout the new connect timeout
	 */
	public void setConnectTimeout(int connectTimeout) {
		if (running) return;
		this.connectTimeout = connectTimeout;
	}
	
	/**
	 * Sets the connect request timeout.
	 *
	 * @param connectRequestTimeout the new connect request timeout
	 */
	public void setConnectRequestTimeout(int connectRequestTimeout) {
		if (running) return;
		this.connectRequestTimeout = connectRequestTimeout;
	}
	
	/**
	 * Sets the max redirects.
	 *
	 * @param maxRedirects the new max redirects
	 */
	public void setMaxRedirects(int maxRedirects) {
		if (running) return;
		this.maxRedirects = maxRedirects;
	}
	
	/**
	 * Get the response as String.
	 *
	 * @return the response
	 */
	public String getResponse() {
		if (running) return null;
		
		if (sb == null) return null;
		return sb.toString();
	}
	
	/**
	 * Execute this DELETE request.
	 */
	public synchronized void execute() {
		if (running) return;
		running = true;
		
		this.response = null;
		this.sb = null;
		this.close();
		
		if (!stringHasValue(getUrl()))
			throw new UnsupportedOperationException("getUrl() returned null! You must specify a URL.");
		
		try {
			this.open();
			
			HttpDelete deleteRequest = new HttpDelete(getUrl());
			
			deleteRequest.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setMaxRedirects(maxRedirects).setConnectTimeout(connectTimeout).build());
			
			if (getCreds() != null)
				if (getCreds() instanceof oauth.signpost.OAuthConsumer) {
					((oauth.signpost.OAuthConsumer)getCreds()).sign(deleteRequest);
				} else {
					if (stringHasValue(getCreds().getPassword()))
						deleteRequest.addHeader(new BasicScheme().authenticate(getCreds(), deleteRequest, null));
				}
			
			this.response = getHttpClient().execute(deleteRequest);


			
			try {
				StringBuilder sb = new StringBuilder();
				
				if (response.getStatusLine().getStatusCode() == 204)
					sb.append("Successful");
				else if (response.getStatusLine().getStatusCode() == 404)
					sb.append("Not Found");
				else
					sb.append("Failed");
				
				this.sb = sb;
			} catch (Throwable e) {}
			
			deleteRequest.releaseConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {  
			this.close();
			running = false;
		}
		
		running = false;
	}
}
